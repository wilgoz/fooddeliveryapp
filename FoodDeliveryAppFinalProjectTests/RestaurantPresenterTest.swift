//
//  RestaurantPresenterTest.swift
//  FoodDeliveryAppFinalProjectTests
//
//  Created by Aya Aurora Rimbamorani on 13/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import XCTest
@testable import FoodDeliveryAppFinalProject
class MockService: IService {
    var stubSuccess: [Restaurant]?
    var stubError: Error?
    func fetchRestaurantList(success: @escaping ([Restaurant]) -> Void, failure: @escaping (Error) -> Void) {
        if let successParameter = stubSuccess {
            success(successParameter)
            return
        }
        if let failureParameter = stubError {
            failure(failureParameter)
            return
        }
    }
}
class MockRestaurantViewDelegate: RestaurantViewDelegate {
    var invokeSuccessCall = 0
    func fetchRestaurantListDidSuccess() {
        invokeSuccessCall += 1
    }
}
class RestaurantPresenterTests: XCTestCase {
    var presenter: RestaurantPresenter!
    var mockService: MockService!
    var mockDelegate: MockRestaurantViewDelegate!
    let mockRestaurant1 = Restaurant(location: "1110",
                                    active: true,
                                    title: "Bakso1",
                                    id: "122",
                                    menuItems: [],
                                    address: "Jakarta",
                                    imageUrl: "",
                                    cuisines: [],
                                    avgSpendLevel: nil,
                                    rating: nil,
                                    deliveryStatus: nil)
    let mockRestaurant2 = Restaurant(location: "1111",
                                    active: true,
                                    title: "Bakso2",
                                    id: "123",
                                    menuItems: [],
                                    address: "Jakarta",
                                    imageUrl: "",
                                    cuisines: [],
                                    avgSpendLevel: nil,
                                    rating: nil,
                                    deliveryStatus: nil)

    override func setUp() {
        mockService = MockService()
        mockDelegate = MockRestaurantViewDelegate()
        presenter = RestaurantPresenter()
        presenter.setRestaurantViewDelegate(mockDelegate)
        presenter.setService(mockService)
        presenter.cartManager.resetCart()
        presenter.favouritesManager.resetFavourites()
    }
    
    override func tearDown() {
        presenter.cartManager.resetCart()
        presenter.cartManager.saveCart()
        presenter.favouritesManager.resetFavourites()
        presenter.favouritesManager.saveFavourites()
    }
    
    func testGivenSuccessResponseFetchRestaurantListGetsRestaurantsData() {
        mockService.stubSuccess = [Restaurant.defaultRestaurant]
        presenter.fetchRestaurantList()
        XCTAssertTrue(presenter.restaurants.count != 0)
        XCTAssertTrue(mockDelegate.invokeSuccessCall == 1)
    }
    
    func testGivenErrorResponseFetchRestaurantListReturnsError() {
        mockService.stubError = NSError(domain: kCFErrorDomainCFNetwork as String, code: 404, userInfo: nil)
        presenter.fetchRestaurantList()
        XCTAssertTrue(presenter.restaurants.count == 0)
    }
    
    func testGivenRestaurantShowMenuItemPageReturnMenuItemViewController() {
        let viewController = presenter.showMenuItemPage(restaurant: mockRestaurant1)
        XCTAssertNotNil(viewController)
    }
    
    func testGivenAFavRestaurantAndIsAddedTrueUpdateRestaurantFavListSetFavouriteManagerToHaveOneFavResto() {
        presenter.updateRestaurantFavList(restoFav: mockRestaurant1, isAdded: true)
        XCTAssertTrue(presenter.favouritesManager.favourites.count != 0)
    }
    
    func testGivenTwoFavRestaurantsWithIsAddedTrueAndIsAddedFalseUpdateRestaurantFavListSetFavouriteManagerToHaveOneFavResto() {
            presenter.updateRestaurantFavList(restoFav: mockRestaurant2, isAdded: true)
            presenter.updateRestaurantFavList(restoFav: mockRestaurant1, isAdded: false)
            XCTAssertTrue(presenter.favouritesManager.favourites.count == 1)
    }
}
