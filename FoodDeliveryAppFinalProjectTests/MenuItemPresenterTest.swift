//
//  MenuItemPresenterTest.swift
//  FoodDeliveryAppFinalProjectTests
//
//  Created by Aya Aurora Rimbamorani on 13/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import XCTest
@testable import FoodDeliveryAppFinalProject

class MenuItemPresenterTest: XCTestCase {
    
    var presenter: MenuItemPresenter!
    
    override func setUp() {
        presenter = MenuItemPresenter()
    }

    override func tearDown() {
    }

    func testGivenRestaurantWithTitleBaksoSetRestaurantSetPresenterRestaurant() {
        let mockRestaurant = Restaurant(location: "1111",
                                        active: true,
                                        title: "Bakso",
                                        id: "123",
                                        menuItems: [],
                                        address: "Jakarta",
                                        imageUrl: "",
                                        cuisines: [],
                                        avgSpendLevel: nil,
                                        rating: nil,
                                        deliveryStatus: nil)
        presenter.setRestaurant(restaurant: mockRestaurant)
        XCTAssertTrue(presenter.restaurant.title == "Bakso")
    }
    
    func testGivenShowCheckoutPageCalledReturnCheckoutViewController() {
        let viewController = presenter.showCheckoutPage()
        XCTAssertNotNil(viewController)
    }
    
    func testGivenIndexPathAndAddedTrueHandleItemButtonTapsAddItemToCart() {
        let mockIndexPath = IndexPath(row: 0, section: 0)
        let mockAdded = true
        let mockMenuItem = MenuItem(description: "Bakso",
                                    id: "1",
                                    restaurantId: "123",
                                    price: 10000,
                                    image: "",
                                    inStock: true,
                                    name: "Bakso")
        let mockRestaurant = Restaurant(location: "1111",
                                        active: true,
                                        title: "Bakso",
                                        id: "123",
                                        menuItems: [mockMenuItem],
                                        address: "Jakarta",
                                        imageUrl: "",
                                        cuisines: [],
                                        avgSpendLevel: nil,
                                        rating: nil,
                                        deliveryStatus: nil)
        presenter.setRestaurant(restaurant: mockRestaurant)
        presenter.handleMenuItemButtonTaps(mockIndexPath, mockAdded)
        XCTAssertTrue(presenter.cartManager.cart.items.count == 1)
    }
}
