//
//  CheckoutPresenterTest.swift
//  FoodDeliveryAppFinalProjectTests
//
//  Created by Aya Aurora Rimbamorani on 13/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import XCTest
@testable import FoodDeliveryAppFinalProject

class MockCheckoutViewDelegate: CheckoutViewDelegate {
    var invokeConfigurePriceLabel = 0
    var invokeConfigureOrderAcceptedView = 0
    
    func configurePriceLabel(price: Int) {
        invokeConfigurePriceLabel += 1
    }
    
    func configureOrderAcceptedView(isAppeared: Bool) {
        invokeConfigureOrderAcceptedView += 1
    }
}

class CheckoutPresenterTest: XCTestCase {
    
    var presenter: CheckoutPresenter!
    var mockCheckoutViewDelegate: MockCheckoutViewDelegate!
    
    override func setUp() {
        presenter = CheckoutPresenter()
        mockCheckoutViewDelegate = MockCheckoutViewDelegate()
        presenter.setCheckoutViewDelegate(mockCheckoutViewDelegate)
    }

    override func tearDown() {
    }

    func testGivenPresenterHasLoadedCartEmptyCartSetPresenterCartEmpty() {
        let mockCart = CartManager.shared
        let mockMenuItem = MenuItem(description: "Bakso",
                                    id: "1",
                                    restaurantId: "123",
                                    price: 10000,
                                    image: "",
                                    inStock: true,
                                    name: "Bakso")
        let mockRestaurant = Restaurant(location: "1111",
                                        active: true,
                                        title: "Bakso",
                                        id: "123",
                                        menuItems: [mockMenuItem],
                                        address: "Jakarta",
                                        imageUrl: "",
                                        cuisines: [],
                                        avgSpendLevel: nil,
                                        rating: nil,
                                        deliveryStatus: nil)
        mockCart.updateCart(mockRestaurant, mockMenuItem, true)
        presenter.emptyCart()
        XCTAssertTrue(presenter.cartManager.cart.items.count == 0)
    }
    
    func testGivenItemAddedToCartHandleMenuItemButtonTapsUpdatePresenterCart() {
        let mockKey = "Bakso1"
        let mockAdded = true
        let mockCart = CartManager.shared
        let mockMenuItem1 = MenuItem(description: "Bakso1",
                                    id: "1",
                                    restaurantId: "123",
                                    price: 10000,
                                    image: "",
                                    inStock: true,
                                    name: "Bakso2")
        let mockMenuItem2 = MenuItem(description: "Bakso2",
                                    id: "2",
                                    restaurantId: "123",
                                    price: 10000,
                                    image: "",
                                    inStock: true,
                                    name: "Bakso1")
        let mockRestaurant = Restaurant(location: "1111",
                                        active: true,
                                        title: "Bakso",
                                        id: "123",
                                        menuItems: [mockMenuItem1, mockMenuItem2],
                                        address: "Jakarta",
                                        imageUrl: "",
                                        cuisines: [],
                                        avgSpendLevel: nil,
                                        rating: nil,
                                        deliveryStatus: nil)
        mockCart.resetCart()
        mockCart.updateCart(mockRestaurant, mockMenuItem1, true)
        mockCart.updateCart(mockRestaurant, mockMenuItem2, true)
        presenter.handleMenuItemButtonTaps(mockKey, mockAdded)
        
        XCTAssertTrue(mockCheckoutViewDelegate.invokeConfigurePriceLabel == 1)
        XCTAssertTrue(presenter.cartManager.cart.items[mockKey]?.totalItems == 2)
    }
    
    func testGivenOrderButtonTappedHandleOrderButtonTappedConfigureOrderAcceptedView() {
        presenter.handleOrderButtonTap(isOrderAccepted: true)
        
        XCTAssertTrue(mockCheckoutViewDelegate.invokeConfigureOrderAcceptedView == 1)
    }
    
    func testGivenShowCheckoutPageCalledReturnCheckoutViewController() {
        let viewController = presenter.showCheckoutPage()
        
        XCTAssertNotNil(viewController)
    }
}
