//
//  FavouritesManager.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Willy Ghozali on 14/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

protocol IFavouritesManager: class {
    var favourites: [Restaurant] { get set }

    func loadFavourites()
    func saveFavourites()
}

final class FavouritesManager: IFavouritesManager {

    static let shared = FavouritesManager()
    private var dataManager = DataManager.shared
    var favourites: [Restaurant] = [Restaurant]()

    private init() {}

    func loadFavourites() {
        favourites = dataManager.loadFavourites()
    }

    func saveFavourites() {
        dataManager.saveFavourites(favourites: favourites)
    }
    
    func resetFavourites() {
        favourites = [Restaurant]()
    }
}
