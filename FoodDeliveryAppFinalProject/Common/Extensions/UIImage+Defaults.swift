//
//  UIImage+Defaults.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Willy Ghozali on 14/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    static let defaultImage = #imageLiteral(resourceName: "No Picture")
}
