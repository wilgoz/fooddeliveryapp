//
//  Service.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Willy Ghozali on 11/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

protocol IService: class {
    func fetchRestaurantList(success: @escaping ([Restaurant]) -> Void, failure: @escaping (Error) -> Void)
}

class Service: IService {

    var networkLayer = NetworkLayer.shared

    func fetchRestaurantList(success: @escaping ([Restaurant]) -> Void, failure: @escaping (Error) -> Void) {
        networkLayer.fetchRestaurants(successHandler: { (restaurants) in
            success(restaurants.restaurants)
        }) { (error) in
            print(error)
        }
    }
}
