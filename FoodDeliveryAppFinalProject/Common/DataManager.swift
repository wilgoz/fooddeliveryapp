//
//  DataManager.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Willy Ghozali on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

protocol IDataManager {
    func loadCart() -> Cart
    func saveCart(cart: Cart)
    func loadFavourites() -> [Restaurant]
    func saveFavourites(favourites: [Restaurant])
}

extension Data {
    var prettyPrintedJSONString: NSString? {
        guard
            let object = try? JSONSerialization.jsonObject(with: self, options: []),
            let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
            let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }
        return prettyPrintedString
    }
}

final class DataManager: IDataManager {

    static var shared = DataManager()
    private init() {}

    func saveCart(cart: Cart) {
        guard let data = try? JSONEncoder().encode(cart) else {
            return
        }
        debugPrint(data.prettyPrintedJSONString!)
        UserDefaults.standard.set(data, forKey: "user_cart")
    }

    func loadCart() -> Cart {
        let emptyCart = Cart.defaultCart
        guard let data = UserDefaults.standard.object(forKey: "user_cart") as? Data else {
            return emptyCart
        }
        if let cart = try? JSONDecoder().decode(Cart.self, from: data) {
            return cart
        }
        return emptyCart
    }

    func saveFavourites(favourites: [Restaurant]) {
        guard let data = try? JSONEncoder().encode(favourites) else {
            return
        }
        UserDefaults.standard.set(data, forKey: "user_favs")
    }

    func loadFavourites() -> [Restaurant] {
        let emptyFavourites = [Restaurant]()
        guard let data = UserDefaults.standard.object(forKey: "user_favs") as? Data else {
            return emptyFavourites
        }
        if let favourites = try? JSONDecoder().decode([Restaurant].self, from: data) {
            return favourites
        }
        return emptyFavourites
    }
}
