//
//  Restaurants.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

struct Restaurants: Codable {
    var restaurants: [Restaurant]
    
    enum CodingKeys: String, CodingKey {
        case restaurants = "dishes"
    }
}
