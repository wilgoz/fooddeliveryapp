//
//  MenuItem.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

struct MenuItem : Codable, Equatable {
    
    let description : String?
    let id : String
    let restaurantId : String
    let price : Int
    let image : String?
    let inStock : Bool?
    let name : String

    enum CodingKeys: String, CodingKey {
        case description = "description"
        case id = "id"
        case restaurantId = "restaurant_id"
        case price = "price"
        case image = "image"
        case inStock = "in_stock"
        case name = "name"
    }
}
