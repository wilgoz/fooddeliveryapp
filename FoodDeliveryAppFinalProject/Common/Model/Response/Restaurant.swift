//
//  Restaurant.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

struct Restaurant: Codable, Equatable {
    
    let location: String?
    let active: Bool?
    let title: String?
    let id: String?
    let menuItems: [MenuItem]?
    let address: String?
    let imageUrl: String?
    let cuisines: [Cuisine]?
    let avgSpendLevel: AverageSpendLevel?
    let rating: Rating?
    let deliveryStatus: DeliveryStatus?

    enum CodingKeys: String, CodingKey {
        case location = "location"
        case active = "active"
        case title = "title"
        case id = "id"
        case menuItems = "menu_items"
        case address = "address"
        case imageUrl = "image_url"
        case cuisines = "cuisines"
        case avgSpendLevel = "avg_spend_level"
        case rating = "rating"
        case deliveryStatus = "delivery_status"
    }

    static let defaultRestaurant = Restaurant(location: "",
                                              active: false,
                                              title: "",
                                              id: "",
                                              menuItems: [],
                                              address: "",
                                              imageUrl: "",
                                              cuisines: [],
                                              avgSpendLevel: AverageSpendLevel.defaultAverageSpendLevel,
                                              rating: Rating.defaultRating,
                                              deliveryStatus: DeliveryStatus.defaultDeliveryStatus)
}

struct Cuisine: Codable, Equatable {
    let text: String?
    let code: String?
    
    enum CodingKeys: String, CodingKey {
        case text = "text"
        case code = "code"
    }
}

struct AverageSpendLevel: Codable, Equatable {
    let priceLevel: Float?
    let imageUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case priceLevel = "price_level"
        case imageUrl = "image_url"
    }

    static let defaultAverageSpendLevel = AverageSpendLevel(priceLevel: 0, imageUrl: "")
}

struct Rating: Codable, Equatable {
    let text: String?
    let imageUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case text = "text"
        case imageUrl = "image_url"
    }

    static let defaultRating = Rating(text: "", imageUrl: "")
}

struct DeliveryStatus: Codable, Equatable {
    let unit: String?
    let distance: Float?
    
    enum CodingKeys: String, CodingKey {
        case unit = "unit"
        case distance = "distance"
    }

    static let defaultDeliveryStatus = DeliveryStatus(unit: "", distance: 0)
}
