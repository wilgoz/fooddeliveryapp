//
//  Cart.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

struct Cart: Codable {
    var restaurant: Restaurant
    var totalPrice: Int
    var items: [String: Item]

    static let defaultCart = Cart(restaurant: Restaurant.defaultRestaurant, totalPrice: 0, items: [:])
}

struct Item: Codable {
    let description: String?
    let id: String
    let restaurantId: String
    let price: Int
    let image: String?
    let inStock: Bool?
    var totalItems: Int

    enum CodingKeys: String, CodingKey {
        case description = "description"
        case id = "id"
        case restaurantId = "restaurant_id"
        case price = "price"
        case image = "image"
        case inStock = "in_stock"
        case totalItems = "total_items"
    }
}
