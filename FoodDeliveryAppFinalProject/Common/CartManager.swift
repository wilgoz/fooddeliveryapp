//
//  CartManager.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Willy Ghozali on 12/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

protocol ICartManager: class {
    var cart: Cart { get }

    func loadCart()
    func resetCart()
    func updateCart(_ restaurant: Restaurant, _ menuItem: MenuItem, _ added: Bool)
    func saveCart()
}

final class CartManager: ICartManager {

    static let shared = CartManager()
    var cart = Cart.defaultCart
    private var dataManager = DataManager.shared

    private init() {}

    func loadCart() {
        cart = dataManager.loadCart()
    }

    func resetCart() {
        cart = Cart.defaultCart
    }

    func updateCart(_ restaurant: Restaurant, _ menuItem: MenuItem, _ added: Bool) {
        let price = added ? menuItem.price : menuItem.price * -1
        if cart.restaurant != restaurant { resetCart() }
        cart.restaurant = restaurant
        if cart.items[menuItem.name] == nil {
            cart.items[menuItem.name] = Item(description: menuItem.description,
                                             id: menuItem.id,
                                             restaurantId: menuItem.restaurantId,
                                             price: menuItem.price,
                                             image: menuItem.image,
                                             inStock: menuItem.inStock,
                                             totalItems: 1)
        } else {
            cart.items[menuItem.name]?.totalItems += added ? 1 : -1
            if cart.items[menuItem.name]?.totalItems == 0 { cart.items.removeValue(forKey: menuItem.name) }
        }
        cart.totalPrice += price
    }

    func saveCart() {
        dataManager.saveCart(cart: cart)
    }
}
