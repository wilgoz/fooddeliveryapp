//
//  ImageLoader.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Willy Ghozali on 14/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation
import UIKit
import Combine

class ImageLoader {

    static let shared = ImageLoader()

    private let cache: ImageCacheType
    private lazy var backgroundQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 5
        return queue
    }()

    private init() {
        self.cache = ImageCache()
    }

    func loadImage(from urlString: String?, defaultImage: UIImage) -> AnyPublisher<UIImage?, Never> {
        guard
            let url = URL(string: urlString ?? "") else {
            return Just(defaultImage).eraseToAnyPublisher()
        }
        if let image = cache[url] {
            return Just(image).eraseToAnyPublisher()
        }
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { (data, response) -> UIImage? in return UIImage(data: data) }
            .catch { error in return Just(nil) }
            .handleEvents(receiveOutput: { [unowned self] image in
                self.cache[url] = image == nil ? defaultImage : image
            })
            .subscribe(on: backgroundQueue)
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}
