//
//  RestaurantPresenter.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Willy Ghozali on 11/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

protocol IRestaurantPresenter: class {
    var restaurants: [Restaurant] { get }
    var cartManager: CartManager { get }

    func setRestaurantViewDelegate(_ delegate: RestaurantViewDelegate)
    func setService(_ service: IService)
    func fetchRestaurantList()
}

protocol RestaurantViewDelegate: class {
    func fetchRestaurantListDidSuccess()
}

class RestaurantPresenter: IRestaurantPresenter {

    private weak var restoViewDelegate: RestaurantViewDelegate?
    private var service: IService?
    var favouritesManager = FavouritesManager.shared

    var restaurants = [Restaurant]()
    var cartManager = CartManager.shared

    func setRestaurantViewDelegate(_ delegate: RestaurantViewDelegate) {
        self.restoViewDelegate = delegate
    }

    func setService(_ service: IService) {
        self.service = service;
    }

    func fetchRestaurantList() {
        service?.fetchRestaurantList(success: { [weak self] (restaurants) in
            self?.restaurants = restaurants
            self?.restoViewDelegate?.fetchRestaurantListDidSuccess()
        }) { (error) in
        }
    }

    func showMenuItemPage(restaurant: Restaurant) -> MenuItemViewController {
        let viewController = MenuItemViewController()
        viewController.setRestaurant(restaurant: restaurant)
        return viewController
    }
    
    func updateRestaurantFavList(restoFav: Restaurant, isAdded: Bool) {
        if isAdded {
            favouritesManager.favourites.append(restoFav)
        } else {
            favouritesManager.favourites = favouritesManager.favourites.filter{ $0 != restoFav }
        }
        favouritesManager.saveFavourites()
    }
}
