//
//  ViewController.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit

protocol RestaurantCellDelegate: class {
    func refreshMainView()
    func showMenuPageFromRestoFav(restaurant: Restaurant)
}

class ViewController: UIViewController {

    static let RESTO_INDEX = 1

    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var tableView: UITableView!
    
    private var checkoutButton: CheckoutButtonView!
    private var presenter: RestaurantPresenter!

    private var searchActive: Bool = false
    private var filtered: [Restaurant] = []
    private var restoRowItems = [0, 0]

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter = RestaurantPresenter()
        presenter.cartManager.loadCart()
        presenter.setService(Service())
        presenter.setRestaurantViewDelegate(self)
        presenter.fetchRestaurantList()
        presenter.favouritesManager.loadFavourites()

        navItem.title = "Restaurants"
        tableView.register(UINib(nibName: "RestoTableViewCell", bundle: nil), forCellReuseIdentifier: "RestoCell")
        tableView.register(UINib(nibName: "RestoFavTableViewCell", bundle: nil), forCellReuseIdentifier: "RestoFavCell")
        setupCheckoutButton()
    }

    private func setupCheckoutButton() {
        checkoutButton = CheckoutButtonView.getCheckoutButton()
        checkoutButton.setDelegate(delegate: self)
        let views = ["button": checkoutButton]
        view.addSubview(checkoutButton)
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: CheckoutButtonView.hConstraint,
                                                           options: [],
                                                           metrics: nil,
                                                           views: views as [String : Any]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: CheckoutButtonView.vConstraint,
                                                           options: [],
                                                           metrics: nil,
                                                           views: views as [String : Any]))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkoutButton.updateCheckoutButton()
        displayFavSection()
    }

    private func displayFavSection() {
        if presenter.favouritesManager.favourites.count == 0 {
            restoRowItems[0] = 0
        } else {
            restoRowItems[0] = 1
        }
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filtered = searchText == ""
            ? presenter.restaurants
            : presenter.restaurants.filter({ (restaurant) -> Bool in
                    let tmp: NSString = (restaurant.title ?? "") as NSString
                    let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                    return range.location != NSNotFound
            })
        searchActive = searchText != ""
        restoRowItems[ViewController.RESTO_INDEX] = searchActive ? filtered.count : presenter.restaurants.count
        tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return restoRowItems.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restoRowItems[section]
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == ViewController.RESTO_INDEX {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RestoCell", for: indexPath) as! RestoTableViewCell
            let restaurant = searchActive ? filtered[indexPath.row] : presenter.restaurants[indexPath.row]
            cell.setPresenter(presenter)
            cell.setDelegate(self)
            cell.configure(restaurant: restaurant)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RestoFavCell") as! RestoFavTableViewCell
            cell.setPresenter(presenter)
            cell.setDelegate(self)
            cell.refreshCollectionView()
            return cell
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let restaurant = searchActive ? filtered[indexPath.row] : presenter.restaurants[indexPath.row]
        navigationController?.pushViewController(
            presenter.showMenuItemPage(restaurant: restaurant),
            animated: true
        )
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == ViewController.RESTO_INDEX {
            return 116
        } else {
            return 240
        }
    }
}

extension ViewController: RestaurantViewDelegate {
    func fetchRestaurantListDidSuccess() {
        restoRowItems[ViewController.RESTO_INDEX] = presenter.restaurants.count
        tableView.reloadData()
    }
}

extension ViewController: checkoutButtonDelegate {
    func moveToCheckoutPage(vc: CheckoutViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ViewController: RestaurantCellDelegate {
    func refreshMainView() {
        displayFavSection()
        tableView.reloadSections([0], with: .automatic)
    }
    
    func showMenuPageFromRestoFav(restaurant: Restaurant) {
        navigationController?.pushViewController(presenter.showMenuItemPage(restaurant: restaurant),
                                                 animated: true)
    }
}
