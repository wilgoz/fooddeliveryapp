//
//  RestoFavCollectionViewCell.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 14/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit

class RestoFavCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var restoImageView: UIImageView!
    @IBOutlet weak var restoFavCard: UIView!
    @IBOutlet weak var restoTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        restoFavCard.dropShadow(corneredRadius: true)
    }
    
    func setRestoImageView(_ imageUrl: String) {
        _ = ImageLoader.shared
                       .loadImage(from: imageUrl, defaultImage: UIImage.defaultImage)
                       .sink { [weak self] image in self?.restoImageView.image = image }
    }
    
    func setRestoTitleLabel(_ title: String) {
        restoTitleLabel.text = title
    }
}
