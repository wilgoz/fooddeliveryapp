//
//  RestoFavTableViewCell.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 14/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit

class RestoFavTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private weak var restoPresenter: RestaurantPresenter?
    private weak var restoCellDelegate: RestaurantCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.register(UINib(nibName: "RestoFavCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RestoFavCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setPresenter(_ presenter: RestaurantPresenter) {
        restoPresenter = presenter
    }
    
    func setDelegate(_ delegate: RestaurantCellDelegate) {
        restoCellDelegate = delegate
    }
    
    func refreshCollectionView() {
        collectionView.reloadData()
    }
}

extension RestoFavTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return restoPresenter?.favouritesManager.favourites.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RestoFavCell", for: indexPath) as! RestoFavCollectionViewCell
        let restaurant = restoPresenter?.favouritesManager.favourites[indexPath.row]
        cell.setRestoImageView(restaurant?.imageUrl ?? "")
        cell.setRestoTitleLabel(restaurant?.title ?? "")
        return cell
    }
}

extension RestoFavTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        restoCellDelegate?.showMenuPageFromRestoFav(restaurant: restoPresenter?.favouritesManager.favourites[indexPath.row] ?? Restaurant.defaultRestaurant)
    }
}
