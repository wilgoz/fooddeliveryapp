//
//  RestoTableViewCell.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit
import Combine

class RestoTableViewCell: UITableViewCell {

    @IBOutlet weak var restoTitleLabel: UILabel!
    @IBOutlet weak var restoCuisineLabel: UILabel!
    @IBOutlet weak var restoImageView: UIImageView!
    @IBOutlet weak var avgSpendLevelImageView: UIImageView!
    @IBOutlet weak var separatorView: UILabel!
    @IBOutlet weak var ratingImageView: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratingDistanceSeparator: UILabel!
    @IBOutlet weak var ratingDistanceStackView: UIStackView!
    @IBOutlet weak var likeRestoImageView: UIImageView!
   
    private weak var restoPresenter: RestaurantPresenter?
    private weak var restoCellDelegate: RestaurantCellDelegate?
    private var restaurant: Restaurant?
    
    private enum ImageViews: String {
        case restaurant = "restoImageView"
        case rating = "ratingImageView"
        case priceLevel = "avgSpendLevelImageView"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        separatorView.isHidden = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(likeRestoImageTaps(tapGestureRecognizer:)))
        likeRestoImageView.isUserInteractionEnabled = true
        likeRestoImageView.addGestureRecognizer(tapGestureRecognizer)
    }

    @objc func likeRestoImageTaps(tapGestureRecognizer: UITapGestureRecognizer) {
        guard let restaurant = restaurant else { return }
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        var isAdded = false
        if restoPresenter?.favouritesManager.favourites.contains(restaurant) ?? false {
            tappedImage.tintColor = .lightGray
        } else {
            tappedImage.tintColor = .red
            isAdded = true
        }
        restoPresenter?.updateRestaurantFavList(restoFav: restaurant, isAdded: isAdded)
        restoCellDelegate?.refreshMainView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(restaurant: Restaurant) {
        self.restaurant = restaurant
        setFavourites()

        restoTitleLabel.text = restaurant.title ?? ""
        loadImage(of: .restaurant, imageUrl: restaurant.imageUrl)

        setCuisine(cuisines: restaurant.cuisines ?? [],
                   avgSpendLevel: restaurant.avgSpendLevel ?? AverageSpendLevel.defaultAverageSpendLevel)

        if let priceLevelImageUrl = restaurant.avgSpendLevel?.imageUrl {
            loadImage(of: .priceLevel, imageUrl: priceLevelImageUrl)
        }

        setRating(rating: restaurant.rating ?? Rating.defaultRating)
        setDistance(deliveryStatus: restaurant.deliveryStatus ?? DeliveryStatus.defaultDeliveryStatus)
    }
    
    func setPresenter(_ presenter: RestaurantPresenter) {
        restoPresenter = presenter
    }
    
    func setDelegate(_ delegate: RestaurantCellDelegate) {
        restoCellDelegate = delegate
    }
    
    func setRestaurant(_ restaurant: Restaurant) {
        self.restaurant = restaurant
   }

    private func setFavourites() {
        if restoPresenter?.favouritesManager.favourites.contains(restaurant!) ?? false {
            likeRestoImageView.tintColor = .red
        } else {
            likeRestoImageView.tintColor = .lightGray
        }
    }

    private func loadImage(of imageView: ImageViews, imageUrl: String?) {
        let selectedImageView: UIImageView
        switch imageView {
        case .priceLevel:
            selectedImageView = avgSpendLevelImageView
            break
        case .restaurant:
            selectedImageView = restoImageView
            break
        case .rating:
            selectedImageView = ratingImageView
            break
        }
        _ = ImageLoader.shared
                       .loadImage(from: imageUrl, defaultImage: UIImage.defaultImage)
                       .sink { image in selectedImageView.image = image }
    }

    private func setCuisine(cuisines: [Cuisine], avgSpendLevel: AverageSpendLevel) {
        var cuisineString = ""
        for (index, cuisine) in cuisines.enumerated() {
            cuisineString += cuisine.text ?? ""
            if index < (cuisines.count - 2) {
                cuisineString += ", "
            } else if index == (cuisines.count - 2) {
                cuisineString += " & "
            } else if index == (cuisines.count - 1) {
                cuisineString += "  "
            }
            separatorView.isHidden = index == (cuisines.count - 1) && avgSpendLevel != AverageSpendLevel.defaultAverageSpendLevel
                ? false
                : true
        }
        restoCuisineLabel.text = cuisineString
    }

    private func setRating(rating: Rating) {
        if rating.imageUrl != "" {
            loadImage(of: .rating, imageUrl: rating.imageUrl)
            ratingLabel.text = " \(NSString(string: rating.text!))"
            setRatingDistanceStackView(isHidden: false, spacing: 10)
            ratingLabel.isHidden = false
            ratingImageView.isHidden = false
        } else {
            setRatingDistanceStackView(isHidden: true, spacing: 0)
            ratingLabel.isHidden = true
            ratingImageView.isHidden = true
        }
    }

    private func setRatingDistanceStackView(isHidden: Bool, spacing: CGFloat) {
        ratingDistanceSeparator.isHidden = isHidden
        ratingDistanceStackView.spacing = spacing
    }

    private func setDistance(deliveryStatus: DeliveryStatus) {
        distanceLabel.text = "\(NSString(format: "%.1f", deliveryStatus.distance!)) \(deliveryStatus.unit ?? "")"
    }
}
