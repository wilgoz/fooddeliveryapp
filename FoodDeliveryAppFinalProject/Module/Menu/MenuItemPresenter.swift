//
//  MenuItemPresenter.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Willy Ghozali on 12/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

protocol IMenuItemPresenter: class {
    var restaurant: Restaurant { get }
    var cartManager: CartManager { get }

    func showCheckoutPage() -> CheckoutViewController
    func handleMenuItemButtonTaps(_ indexPath: IndexPath, _ added: Bool)
    func setRestaurant(restaurant: Restaurant)
}

class MenuItemPresenter: IMenuItemPresenter {

    var restaurant = Restaurant.defaultRestaurant
    var cartManager = CartManager.shared

    func setRestaurant(restaurant: Restaurant) {
        self.restaurant = restaurant
    }
    
    func showCheckoutPage() -> CheckoutViewController {
        let viewController = CheckoutViewController()
        _ = viewController.view
        viewController.configurePriceLabel(price: cartManager.cart.totalPrice)
        return viewController
    }
    
    func handleMenuItemButtonTaps(_ indexPath: IndexPath, _ added: Bool) {
        if let item = restaurant.menuItems?[indexPath.row] {
            cartManager.updateCart(restaurant, item, added)
            cartManager.saveCart()
        }
    }
}
