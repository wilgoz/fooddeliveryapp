//
//  MenuItemViewController.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit

class MenuItemViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    private var checkoutButton: CheckoutButtonView!
    private var presenter: MenuItemPresenter!

    init() {
        super.init(nibName: "MenuItemViewController", bundle: nil)
        presenter = MenuItemPresenter()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCheckoutButton()
        collectionView.register(UINib(nibName: "MenuItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MenuCell")
        collectionView.register(UINib(nibName: "MenuHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "MenuHeader")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkoutButton.updateCheckoutButton()
        collectionView.reloadData()
    }

    func setRestaurant(restaurant: Restaurant) {
        presenter.setRestaurant(restaurant: restaurant)
    }

    private func setupCheckoutButton() {
        checkoutButton = CheckoutButtonView.getCheckoutButton()
        checkoutButton.setDelegate(delegate: self)
        let views = ["button": checkoutButton]
        view.addSubview(checkoutButton)
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: CheckoutButtonView.hConstraint,
                                                           options: [],
                                                           metrics: nil,
                                                           views: views as [String : Any]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: CheckoutButtonView.vConstraint,
                                                           options: [],
                                                           metrics: nil,
                                                           views: views as [String : Any]))
   }
}

extension MenuItemViewController: CounterItemCellDelegate {
    func addButtonTapped(_ indexPath: IndexPath) {
        presenter.handleMenuItemButtonTaps(indexPath, true)
        checkoutButton.updateCheckoutButton()
    }

    func subButtonTapped(_ indexPath: IndexPath) {
        presenter.handleMenuItemButtonTaps(indexPath, false)
        checkoutButton.updateCheckoutButton()
    }
}

extension MenuItemViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.restaurant.menuItems?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! MenuItemCollectionViewCell
        let item = presenter.restaurant.menuItems![indexPath.row]
        let cart = presenter.cartManager.cart
        let counter = cart.items[item.name] != nil && cart.restaurant == presenter.restaurant
            ? cart.items[item.name]!.totalItems
            : 0
        cell.configure(item: item, indexPath: indexPath, counter: counter)
        cell.setDelegate(delegate: self)
        return cell
    }
}

extension MenuItemViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 100)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MenuHeader", for: indexPath) as! MenuHeaderCollectionReusableView
            headerView.configure(restaurant: presenter.restaurant)
            return headerView
        default:
            assert(false, "Invalid element type")
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 260)
    }
}

extension MenuItemViewController: checkoutButtonDelegate {
    func moveToCheckoutPage(vc: CheckoutViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
}
