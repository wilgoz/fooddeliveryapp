//
//  MenuHeaderCollectionReusableView.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit

class MenuHeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var restoImageView: UIImageView!
    @IBOutlet weak var restoTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.backgroundColor = UIColor.lightGray.cgColor
    }

    func configure(restaurant: Restaurant) {
        restoTitleLabel.text = restaurant.title
        _ = ImageLoader.shared
                       .loadImage(from: restaurant.imageUrl, defaultImage: UIImage.defaultImage)
                       .sink { [weak self] image in self?.restoImageView.image = image }
    }
}
