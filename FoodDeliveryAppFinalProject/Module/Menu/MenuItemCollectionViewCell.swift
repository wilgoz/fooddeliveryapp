//
//  MenuItemCollectionViewCell.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit

protocol CounterItemCellDelegate: class {
    func addButtonTapped(_ indexPath: IndexPath)
    func subButtonTapped(_ indexPath: IndexPath)
}

class MenuItemCollectionViewCell: UICollectionViewCell {

    private weak var delegate: CounterItemCellDelegate?
    private var indexPath: IndexPath!
    private var counter = 0 {
        didSet {
            configButtons()
        }
    }

    @IBOutlet weak var menuTitleLabel: UILabel!
    @IBOutlet weak var menuDescriptionLabel: UILabel!
    @IBOutlet weak var menuPriceLabel: UILabel!
    @IBOutlet weak var menuImageView: UIImageView!

    @IBOutlet weak var quantButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var subButton: UIButton!

    @IBAction func quantButtonTapped(_ sender: Any) { doAdd() }
    @IBAction func addButtonTapped(_ sender: Any) { doAdd() }
    @IBAction func subButtonTapped(_ sender: Any) {
        if counter == 0 {
            doAdd()
        } else {
            counter -= 1
            delegate?.subButtonTapped(indexPath)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setDelegate(delegate: CounterItemCellDelegate) {
        self.delegate = delegate
    }

    func configure(item: MenuItem, indexPath: IndexPath, counter: Int) {
        self.indexPath = indexPath
        self.counter = counter
        menuPriceLabel.text = "Rp. \(String(describing: item.price))"
        menuTitleLabel.text = item.name
        menuDescriptionLabel.text = item.description
        _ = ImageLoader.shared
                       .loadImage(from: item.image, defaultImage: UIImage.defaultImage)
                       .sink { [weak self] image in self?.menuImageView.image = image }
    }

    private func doAdd() {
        counter += 1
        delegate?.addButtonTapped(indexPath)
    }

    private func configButtons() {
        if counter > 0 {
            quantButton.changeButtonColor(bgColor: .white, titleColor: .black)
            subButton.changeButtonColor(bgColor: .white, titleColor: UIColor.systemGreen)
            addButton.changeButtonColor(bgColor: .white, titleColor: UIColor.systemGreen)
            quantButton.isEnabled = false
            quantButton.setTitle(String(counter), for: .normal)
            subButton.setTitle("-", for: .normal)
        } else {
            quantButton.changeButtonColor(bgColor: UIColor.systemGreen, titleColor: .white)
            subButton.changeButtonColor(bgColor: UIColor.systemGreen, titleColor: .white)
            addButton.changeButtonColor(bgColor: UIColor.systemGreen, titleColor: .white)
            subButton.setTitle(" ", for: .normal)
            quantButton.isEnabled = true
            quantButton.setTitle("Add", for: .normal)
        }
    }
}
