//
//  OrderItemCollectionViewCell.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 11/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit

protocol OrderCellDelegate: class {
    func addButtonTapped(_ key: String)
    func subButtonTapped(_ key: String)
    func reloadCheckoutView()
}

class OrderItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var menuTitleLabel: UILabel!
    @IBOutlet weak var menuPriceLabel: UILabel!
    @IBOutlet weak var menuImageView: UIImageView!
    
    @IBOutlet weak var subButton: UIButton!
    @IBOutlet weak var quantLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    private weak var delegate: OrderCellDelegate?
    private var counter = 0 {
        didSet {
            configLabel()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func addButtonTapped(_ sender: Any) { doAdd() }
    @IBAction func subButtonTapped(_ sender: Any) { doSub() }
    
    private func doAdd() {
        counter += 1
        delegate?.addButtonTapped(menuTitleLabel.text!)
    }
    
    private func doSub() {
        counter -= 1
        if counter == 0 {
            delegate?.reloadCheckoutView()
        }
        delegate?.subButtonTapped(menuTitleLabel.text!)
    }
    
    func setDelegate(delegate: OrderCellDelegate) {
        self.delegate = delegate
    }

    func configure(item: (key: String, value: Item)) {
        setMenuTitle(title: item.key)
        setMenuPrice(price: item.value.price)
        setMenuImage(imageUrl: item.value.image)
        setCounter(counter: item.value.totalItems)
    }

    private func configLabel() {
        quantLabel.text = String(counter)
    }

    private func setMenuTitle(title: String) {
        menuTitleLabel.text = title
    }

    private func setMenuPrice(price: Int) {
        menuPriceLabel.text = "Rp. \(String(describing: price))"
    }

    private func setMenuImage(imageUrl: String?) {
        _ = ImageLoader.shared
                       .loadImage(from: imageUrl, defaultImage: UIImage.defaultImage)
                       .sink { [weak self] image in self?.menuImageView.image = image }
    }

    private func setCounter(counter: Int) {
        self.counter = counter
    }
}
