//
//  CheckoutViewController.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 11/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit

protocol CheckoutDelegate: class {
    func updateCheckoutButton()
}

class CheckoutViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var finalOrderView: UIView!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var orderAcceptedView: UIView!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    
    private var presenter: CheckoutPresenter!
    private var isOrderAccepted = false

    init() {
        super.init(nibName: "CheckoutViewController", bundle: nil)
        presenter = CheckoutPresenter()
        presenter.setCheckoutViewDelegate(self)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        finalOrderView.dropShadow(corneredRadius: true)
        collectionView.register(UINib(nibName: "OrderItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OrderItemCell")
        collectionView.register(UINib(nibName: "CheckoutHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "CheckoutHeader")
        configureOrderAcceptedView(isAppeared: isOrderAccepted)
    }
    
    @IBAction func cancelOrderPressed(_ sender: UIButton) {
        presenter.emptyCart()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func orderButtonPressed(_ sender: UIButton) {
        presenter.handleOrderButtonTap(isOrderAccepted: true)
    }
    
    @IBAction func yesButtonPressed(_ sender: UIButton) {
        presenter.emptyCart()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func noButtonPressed(_ sender: UIButton) {
        presenter.handleOrderButtonTap(isOrderAccepted: false)
    }
}

extension CheckoutViewController: CheckoutViewDelegate {
    func configurePriceLabel(price: Int) {
        totalPriceLabel.text = "Rp. \(String(price))"
        if price == 0 {
            navigationController?.popViewController(animated: true)
        }
    }
    
    func configureOrderAcceptedView(isAppeared: Bool) {
        if isAppeared {
            orderAcceptedView.isHidden = false
            finalOrderView.isUserInteractionEnabled = false
            collectionView.isUserInteractionEnabled = false
            orderAcceptedView.dropShadow(corneredRadius: true)
            yesButton.dropShadow(corneredRadius: true)
            noButton.dropShadow(corneredRadius: true)
        } else {
            orderAcceptedView.isHidden = true
            finalOrderView.isUserInteractionEnabled = true
            collectionView.isUserInteractionEnabled = true
        }
    }
}

extension CheckoutViewController: OrderCellDelegate {
    func addButtonTapped(_ key: String) {
        presenter.handleMenuItemButtonTaps(key, true)
    }

    func subButtonTapped(_ key: String) {
        presenter.handleMenuItemButtonTaps(key, false)
    }
    
    func reloadCheckoutView() {
        self.collectionView.reloadData()
    }
}

extension CheckoutViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.cartManager
                        .cart
                        .items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = Array(presenter.cartManager
                                  .cart
                                  .items)[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderItemCell", for: indexPath) as! OrderItemCollectionViewCell
        cell.setDelegate(delegate: self)
        cell.configure(item: item)
        return cell
    }
}

extension CheckoutViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 100)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CheckoutHeader", for: indexPath) as! CheckoutHeaderCollectionReusableView
            headerView.configure(restoName: presenter.cartManager.cart.restaurant.title!)
            return headerView
        default:
            assert(false, "Invalid element type")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 30)
    }
}
