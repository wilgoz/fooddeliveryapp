//
//  CheckoutHeaderCollectionReusableView.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 13/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit

class CheckoutHeaderCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var restaurantName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(restoName: String) {
        restaurantName.text = restoName
    }
}
