//
//  CheckoutButtonView.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Willy Ghozali on 13/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import UIKit

protocol checkoutButtonDelegate: class {
    func moveToCheckoutPage(vc: CheckoutViewController)
}

class CheckoutButtonView: UIView {

    @IBOutlet weak var restoNameLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!

    static let hConstraint = "H:|-40-[button]-40-|"
    static let vConstraint = "V:[button(80)]-40-|"

    private weak var delegate: checkoutButtonDelegate?
    private var presenter: CheckoutPresenter!
    private var checkoutButtonTappedNotifier: Bool = false {
        didSet {
            delegate?.moveToCheckoutPage(vc: presenter.showCheckoutPage())
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        if let view = Bundle.main.loadNibNamed("CheckoutButtonView", owner: self, options: nil)?.first as? UIView {
            addSubview(view)
            view.frame = self.bounds
            view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            let tapGest = UITapGestureRecognizer(target: self, action: #selector(checkoutButtonTapped))
            view.addGestureRecognizer(tapGest)
            presenter = CheckoutPresenter()
        }
    }

    static func getCheckoutButton() -> CheckoutButtonView {
        let checkoutButton = CheckoutButtonView(frame: .zero)
        checkoutButton.dropShadow(corneredRadius: false)
        checkoutButton.translatesAutoresizingMaskIntoConstraints = false
        return checkoutButton
    }

    func setDelegate(delegate: checkoutButtonDelegate) {
        self.delegate = delegate
    }

    func updateCheckoutButton() {
        let cart = presenter.cartManager.cart
        if cart.items.count == 0 {
            isHidden = true
            restoNameLabel.text = ""
            itemPriceLabel.text = ""
        } else {
            isHidden = false
            let totalItems = cart.items.map {$1.totalItems}.reduce(0, +)
            restoNameLabel.text = cart.restaurant.title!
            itemPriceLabel.text = "\(totalItems) \(totalItems > 1 ? "items" : "item") | \(cart.totalPrice)"
        }
    }

    @objc func checkoutButtonTapped() {
        checkoutButtonTappedNotifier = !checkoutButtonTappedNotifier
    }
}
