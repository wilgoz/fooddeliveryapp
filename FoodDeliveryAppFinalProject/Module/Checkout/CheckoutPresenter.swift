//
//  CheckoutPresenter.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Willy Ghozali on 12/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

protocol ICheckoutPresenter: class {
    var cartManager: CartManager { get }

    func setCheckoutViewDelegate(_ delegate: CheckoutViewDelegate)
    func emptyCart()
    func handleMenuItemButtonTaps(_ key: String, _ added: Bool)
    func handleOrderButtonTap(isOrderAccepted: Bool)
    func showCheckoutPage() -> CheckoutViewController
}

protocol CheckoutViewDelegate: class {
    func configurePriceLabel(price: Int)
    func configureOrderAcceptedView(isAppeared: Bool)
}

class CheckoutPresenter: ICheckoutPresenter {

    private weak var checkoutViewDelegate: CheckoutViewDelegate?
    var cartManager = CartManager.shared
    
    func setCheckoutViewDelegate(_ delegate: CheckoutViewDelegate) {
        checkoutViewDelegate = delegate
    }
    
    func emptyCart() {
        cartManager.resetCart()
        cartManager.saveCart()
    }
    
    func handleMenuItemButtonTaps(_ key: String, _ added: Bool) {
        let restaurant = cartManager.cart.restaurant
        var index = 0
        while (restaurant.menuItems?[index].name != key) {
            index += 1
        }
        if let menuItem = restaurant.menuItems?[index] {
            cartManager.updateCart(restaurant, menuItem, added)
            cartManager.saveCart()
        }
        checkoutViewDelegate?.configurePriceLabel(price: cartManager.cart.totalPrice)
    }
    
    func handleOrderButtonTap(isOrderAccepted: Bool) {
        checkoutViewDelegate?.configureOrderAcceptedView(isAppeared: isOrderAccepted)
    }

    func showCheckoutPage() -> CheckoutViewController {
        let viewController = CheckoutViewController()
        _ = viewController.view
        viewController.configurePriceLabel(price: cartManager.cart.totalPrice)
        return viewController
    }
}
