//
//  NetworkLayer.swift
//  FoodDeliveryAppFinalProject
//
//  Created by Aya Aurora Rimbamorani on 10/04/20.
//  Copyright © 2020 Aya Aurora R & Willy Ghozali. All rights reserved.
//

import Foundation

class NetworkLayer {

    static var shared = NetworkLayer()
    private init() {}
    
    func fetchRestaurants(successHandler: @escaping (Restaurants) -> Void, errorHandler: @escaping (Error) -> Void) {
        let session = URLSession.shared
        let urlRequest = URLRequest(url: URL(string: "http://www.mocky.io/v2/5e952bef31000097be5e36ec")!)
        session.dataTask(with: urlRequest) { (data,response,error) in
            guard error == nil else {
                return
            }
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode >= 200 && httpResponse.statusCode < 300 else {
                return
            }
            guard let data = data else {
                DispatchQueue.main.async {
                    errorHandler(NSError(domain: "", code: 0, userInfo: nil))
                }
                return
            }
            do {
                let restaurantList = try JSONDecoder().decode(RestaurantData.self, from: data)
                DispatchQueue.main.async {
                    successHandler(restaurantList.data)
                }
            } catch {
            }
        }.resume()
    }
}
